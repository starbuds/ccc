/* global jQuery */

jQuery(function ($) {
  $('body').addClass('loaded')

  //init controller
  var controller = new ScrollMagic.Controller();

  // Intro animations
  $('.fade-in').each(function() {
    new ScrollMagic.Scene({
      triggerElement: $(this)[0]
    })
      .setClassToggle($(this)[0], 'fade-up')
      .triggerHook(0.7)
      .addTo(controller)
  })


  // Popup + Cookies

  $('.header .highlight').find('a').addClass('input-health')

  if (Cookies.get('province')) {
    $('.header .selector .current').text(Cookies.get('province').toUpperCase())
    $('.header .selector').fadeIn()
  } else {
  //  $('.popup').fadeIn()
  }

  $('.popup__form').submit(function(e) {
    e.preventDefault()
    var province = $('.popup__select').val()
    updateProvince(province)
    if(Cookies.get('province')) {
      $('.popup').fadeOut();
      $('.header .selector').fadeIn()
    }
  })

  $('.selector .current__outer').click(function() {
    if($('.selector .selector__list').is(':visible')) {
      $('.selector .selector__list').fadeOut()
    } else {
      $('.selector .selector__list').fadeIn('fast')
    }
  })

  $('.selector .selector__option').click( function () {
    updateProvince($(this).attr('data-value'))
    $('.selector .selector__list').fadeOut()
  })

  $('.input-health').click(function(e) {
    e.preventDefault()
    if (Cookies.get('province') !== 'other') {
      //window.open('https://compass'+ Cookies.get('province') +'.inputhealth.com/ebooking#new')
      window.open('/booking/', '_self')
    }
  })

  if (Cookies.get('province') == 'other') {
    $('.input-health').each(function() {
      $(this).attr('disabled', true).attr('title','Not supported in your location')
    })
  }
})

function updateProvince(province) {
  Cookies.set('province', province)
  jQuery('.selector .current').text(province.toUpperCase())
  if (Cookies.get('province') == 'other') {
    jQuery('.input-health').each(function() {
      jQuery(this).attr('disabled', true).attr('title','Not supported in your location')
    })
  } else {
    jQuery('.input-health').each(function() {
      jQuery(this).attr('disabled', false).attr('title','')
    })
  }
}
