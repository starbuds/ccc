'use strict'

const gulp = require('gulp')
const browserSync = require('browser-sync').create()
const postcss = require('gulp-postcss')
const assets = require('postcss-assets')
const sourcemaps = require('gulp-sourcemaps')
const uglify = require('gulp-uglify')
const concat = require('gulp-concat')
const rename = require('gulp-rename')
const imagemin = require('gulp-imagemin')
const del = require('del')

const options = {
  devUrl: 'ccc.dev'
}

gulp.task('css', function () {
  return gulp.src(['./src/postcss/main.css'])
    .pipe(sourcemaps.init())
    .pipe(postcss([
      require('precss'),
      require('lost'),
      require('rucksack-css'),
      require('postcss-custom-media'),
      require('css-mqpacker')({
        sort: true
      }),
      require('autoprefixer')({
        browsers: "last 6 Safari versions, last 2 ie versions"
      }),
      require('cssnano'),
      require('pixrem'),
      assets({
        basePath: 'dist/',
        loadPaths: ['img/']
      })
    ]))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/css/'))
    .pipe(browserSync.stream())
})

gulp.task('img', function () {
  return gulp.src([
    './src/img/**/*.*'])
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/img/'))
    .pipe(browserSync.stream())
})

gulp.task('js', function () {
  return gulp.src([
    './src/js/*.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('app.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/js/'))
    .pipe(browserSync.stream())
})

gulp.task('vendor-css', function () {
  return gulp.src(
    ['./src/css/vendor/normalize.css', './src/css/vendor/font-awesome.min.css',
    './src/css/vendor/*.css'])
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest('./dist/css/vendor'))
})

gulp.task('vendor-js', function () {
  return gulp.src(
    ['./src/js/vendor/jquery*.js', './src/js/vendor/*.js'])
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./dist/js/vendor'))
})

gulp.task('minify-js', ['clean'], function () {
  return gulp.src('./dist/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(rename('app.min.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/js/'))
})

gulp.task('fonts', function () {
  return gulp.src('./src/fonts/*.*')
    .pipe(gulp.dest('./dist/css/fonts'))
})

gulp.task('clean', function () {
  return del(['./dist/css/**/*.*', './dist/js/**/*.*'])
})

gulp.task('watch', ['css', 'js', 'vendor-js', 'vendor-css', 'img', 'fonts'], function () {
  gulp.watch('./src/postcss/**/*.css', ['css'])
  gulp.watch('./src/js/**/*.js', ['js'])
  gulp.watch('./src/img/**/*.*', ['img'])
  gulp.watch(['./views/**/*.twig', './functions.php']).on('change', browserSync.reload)
})

gulp.task('serve', ['clean', 'watch'], function () {
  browserSync.init({
    proxy: options.devUrl,
    notify: false,
    open: false
  })
})

gulp.task('build', ['css', 'js', 'vendor-js', 'vendor-css', 'fonts', 'img'])

gulp.task('default', ['clean', 'build'])
