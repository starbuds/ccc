<?php
/**
 * Represents the view for the public-facing component of the plugin.
 *
 * This typically includes any information, if any, that is rendered to the
 * frontend of the theme when the plugin is activated.
 *
 * @package   BH_Store_Locator
 * @author    Bjorn Holine <bjorn2404@gmail.com>
 * @license   GPL-3.0+
 * @link      https://cardinalwp.com/
 * @copyright 2017 Bjorn Holine
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>

<!-- This file is used to markup the public facing aspect of the plugin. -->
