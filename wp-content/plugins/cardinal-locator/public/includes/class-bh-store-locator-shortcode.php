<?php
/**
 * Shortcode functionality
 *
 * @package BH_Store_Locator
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Class BH_Store_Locator_Shortcode
 */
class BH_Store_Locator_Shortcode {
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
		// Set up settings defaults.
		$this->primary_defaults = BH_Store_Locator_Defaults::get_primary_defaults();
		$this->address_meta_defaults = BH_Store_Locator_Defaults::get_address_defaults();
		$this->filter_defaults = BH_Store_Locator_Defaults::get_filter_defaults();
		$this->style_defaults = BH_Store_Locator_Defaults::get_style_defaults();
		$this->structure_defaults = BH_Store_Locator_Defaults::get_structure_defaults();
		$this->language_defaults = BH_Store_Locator_Defaults::get_language_defaults();
		$this->wp_info = BH_Store_Locator_Defaults::get_wp_info_defaults();

		// Get option values for use.
		$this->primary_option_vals = get_option( 'bh_storelocator_primary_options', $this->primary_defaults );
		$this->address_option_vals = get_option( 'bh_storelocator_meta_options', $this->address_meta_defaults );
		$this->filter_option_vals = get_option( 'bh_storelocator_filter_options', $this->filter_defaults );
		$this->style_option_vals = get_option( 'bh_storelocator_style_options', $this->style_defaults );
		$this->structure_option_vals = get_option( 'bh_storelocator_structure_options', $this->structure_defaults );
		$this->language_option_vals = get_option( 'bh_storelocator_language_options', $this->language_defaults );

		// Only register the shortcodes if there are locations added.
		if ( $this->bh_storelocator_check_post_type_locations() ) {
			// Add custom plugin shortcode.
			add_shortcode( 'cardinal-storelocator', array( $this, 'bh_storelocator_shortcode' ) );
			// Add filters shortcode.
			add_shortcode( 'cardinal-storelocator-filters', array( $this, 'bh_storelocator_filters_shortcode' ) );
			// Single map shortcode.
			add_shortcode( 'cardinal-storelocator-single-map', array( $this, 'bh_storelocator_single_map_shortcode' ) );
		}
	}

	/**
	 * Check to see if any location posts have been added
	 */
	public function bh_storelocator_check_post_type_locations() {
		$display = false;

		// Before query hook.
		do_action( 'bh_sl_before_location_query' );

		if ( 'cpt' === $this->primary_option_vals['datasource'] ) {
			$location_args = array(
				'no_found_rows' => true,
				'post_type' => $this->primary_option_vals['posttype'],
				'posts_per_page' => 1,
				'update_post_meta_cache' => false,
				'update_post_term_cache' => false,
			);

			$location_query = new WP_Query( $location_args );

			if ( count( $location_query->posts ) > 0 ) {
				$display = true;
			}
		} elseif ( 'locations' === $this->primary_option_vals['datasource'] ) {
			$location_args = array(
				'no_found_rows' => true,
				'post_type' => BH_Store_Locator::BH_SL_CPT,
				'posts_per_page' => 1,
				'update_post_meta_cache' => false,
				'update_post_term_cache' => false,
			);

			$location_query = new WP_Query( $location_args );

			if ( count( $location_query->posts ) > 0 ) {
				$display = true;
			}
		} elseif ( 'localfile' === $this->primary_option_vals['datasource'] || 'remoteurl' === $this->primary_option_vals['datasource'] ) {
			$display = true;
		}

		wp_reset_postdata();

		// After query hook.
		do_action( 'bh_sl_after_location_query' );

		return $display;
	}

	/**
	 * Get the values of the custom meta field by key
	 *
	 * @param string $key Meta key to check.
	 *
	 * @return array|void
	 */
	private function bh_storelocator_get_location_meta_vals( $key = '' ) {
		global $wpdb;
		$all_meta_vals = array();

		if ( empty( $key ) ) {
			return;
		}

		$used_meta_vals = wp_cache_get( 'bh_storelocator_meta_vals' . $key );

		if ( false === $used_meta_vals ) {

			$used_meta_vals = $wpdb->get_col(
				$wpdb->prepare( "
			        SELECT DISTINCT pm.meta_value FROM {$wpdb->postmeta} pm
			        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
			        WHERE pm.meta_key = '%s'
			        AND p.post_status = '%s'
			        AND p.post_type = '%s'
			    ",
					$key,
					'publish',
					BH_Store_Locator::BH_SL_CPT
				)
			);

			wp_cache_set( 'bh_storelocator_meta_vals' . $key, $used_meta_vals );
		}

		// Prepare the array to return.
		if ( count( $used_meta_vals ) > 0 ) {
			foreach ( $used_meta_vals as $meta_val ) {
				array_push( $all_meta_vals, $meta_val );
			}
		}

		return $all_meta_vals;
	}

	/**
	 * Get all possible meta/taxonomy values
	 *
	 * @param string $tax The meta or taxnoomy name.
	 *
	 * @return array
	 */
	protected function bh_storelocator_get_tax_meta_vals( $tax ) {
		$vals = array();

		// Initially handle filters that are post meta, otherwise check to see if it's a taxonomy.
		if ( 'city' === $tax ) {
			$vals = $this->bh_storelocator_get_location_meta_vals( '_bh_sl_city' );
		} elseif ( 'state' === $tax ) {
			$vals = $this->bh_storelocator_get_location_meta_vals( '_bh_sl_state' );
		} elseif ( 'postal' === $tax ) {
			$vals = $this->bh_storelocator_get_location_meta_vals( '_bh_sl_postal' );
		} elseif ( taxonomy_exists( $tax ) ) {
			$tax_terms = get_terms( $tax );

			// Return the taxonomy term names.
			foreach ( $tax_terms as $tax_term ) {
				array_push( $vals, $tax_term->name );
			}
		}

		return $vals;
	}

	/**
	 * Set up the select filter markup.
	 *
	 * @param array  $vals Filter values.
	 * @param string $filter_name Filter name.
	 * @param string $filter_label Filter label.
	 *
	 * @return string
	 */
	protected function bh_storelocator_setup_select_filters( $vals, $filter_name, $filter_label ) {
		$filter_output = '';

		$filter_output .= '<ul id="' . $filter_name . '-filters-container" class="bh-sl-filters">';
		$filter_output .= '<li><h3 class="bh-sl-filter-title">' . $filter_label . '</h3></li>';
		$filter_output .= '<li>';
		$filter_output .= '<select name="' . $filter_name .'">';
		$filter_output .= '<option value="">All</option>';

		foreach ( $vals as $val ) {
			if ( null !== $val ) {
				$filter_output .= '<option value="' . $val . '">' . $val . '</option>';
			}
		}

		$filter_output .= '</select>';
		$filter_output .= '</li>';
		$filter_output .= '</ul>';

		return $filter_output;
	}

	/**
	 * Set up the checkbox filter markup.
	 *
	 * @param array  $vals Filter values.
	 * @param string $filter_name Filter name.
	 * @param string $filter_label Filter label.
	 *
	 * @return string
	 */
	protected function bh_storelocator_setup_checkbox_filters( $vals, $filter_name, $filter_label ) {
		$filter_output = '';

		$filter_output .= '<ul id="' . $filter_name . '-filters-container" class="bh-sl-filters">';
		$filter_output .= '<li><h3 class="bh-sl-filter-title">' . $filter_label . '</h3></li>';

		foreach ( $vals as $val ) {
			if ( null !== $val ) {
				$filter_output .= '<li>';
				$filter_output .= '<label>';
				$filter_output .= '<input type="checkbox" name="' . $filter_name . '" value="' . $val . '"> <span class="bh-sl-cat-' . sanitize_title( $val ) . '">' . $val . '</span>';
				$filter_output .= '</label>';
				$filter_output .= '</li>';
			}
		}

		$filter_output .= '</ul>';

		return $filter_output;
	}

	/**
	 * Set up the radio filter markup.
	 *
	 * @param array  $vals Filter values.
	 * @param string $filter_name Filter name.
	 * @param string $filter_label Filter label.
	 *
	 * @return string
	 */
	protected function bh_storelocator_setup_radio_filters( $vals, $filter_name, $filter_label ) {
		$filter_output = '';

		$filter_output .= '<ul id="' . $filter_name . '-filters-container" class="bh-sl-filters">';
		$filter_output .= '<li><h3 class="bh-sl-filter-title">' . $filter_label . '</h3></li>';

		foreach ( $vals as $val ) {
			if ( null !== $val ) {
				$filter_output .= '<li>';
				$filter_output .= '<input type="radio" name="' . $filter_name . '" value="' . $val . '"> <span class="bh-sl-cat-' . sanitize_title( $val ) . '">' . $val . '</span>';
				$filter_output .= '</li>';
			}
		}

		$filter_output .= '</ul>';

		return $filter_output;
	}

	/**
	 * Setup the filter markup
	 *
	 * @return string
	 */
	public function bh_storelocator_shortcode_filters_setup() {
		$html = '';

		// Filters.
		$filter_pairs = array();
		$matching_filter_pairs = array();
		$filter_count = 0;

		$html .= '<div class="' . esc_html( $this->structure_option_vals['taxonomyfilterscontainer'] ) . '">';
		foreach ( $this->filter_option_vals['bhslfilters'] as $filter ) {

			if ( 0 === count( $filter_pairs ) ) {
				$filter_pairs['key'] = $filter;
			} elseif ( 1 === count( $filter_pairs ) ) {
				$filter_pairs['type'] = $filter;
			}

			if ( $filter_count % 2 ) {
				array_unshift( $matching_filter_pairs, $filter_pairs );
				$filter_pairs = array();
			}

			$filter_count++;
		}

		// Array is in the reverse order after the above loop - reversing.
		$matching_filter_pairs = array_reverse( $matching_filter_pairs );

		// Another for loop to output the actual filters.
		foreach ( $matching_filter_pairs as $matching_filter ) {
			// Get all the possible filter values.
			$filter_vals = $this->bh_storelocator_get_tax_meta_vals( $matching_filter['key'] );
			// Sort the array using natural order.
			natsort( $filter_vals );

			// Allow the filter vals to be modified.
			$filter_vals = apply_filters( 'bh_sl_filter_vals_' . $matching_filter['key'], $filter_vals );

			// Filter label setup.
			if ( 'city' === $matching_filter['key'] ) {
				$filter_label = 'City';
			} elseif ( 'state' === $matching_filter['key'] ) {
				$filter_label = 'State';
			} elseif ( 'postal' === $matching_filter['key'] ) {
				$filter_label = 'Postal Code';
			} elseif ( taxonomy_exists( $matching_filter['key'] ) ) {
				$filter_tax = get_taxonomy( $matching_filter['key'] );
				$filter_label = $filter_tax->labels->name;
			}

			// Output the appropriate filter markup.
			if ( count( $filter_vals ) > 0 ) {
				if ( 'select' === $matching_filter['type'] ) {
					$html .= $this->bh_storelocator_setup_select_filters( $filter_vals, $matching_filter['key'], $filter_label );
				} elseif ( 'checkbox' === $matching_filter['type'] ) {
					$html .= $this->bh_storelocator_setup_checkbox_filters( $filter_vals, $matching_filter['key'], $filter_label );
				} elseif ( 'radio' === $matching_filter['type'] ) {
					$html .= $this->bh_storelocator_setup_radio_filters( $filter_vals, $matching_filter['key'], $filter_label );
				}
			}
		}

		$html .= '</div>';

		return $html;
	}

	/**
	 * Shortcode
	 *
	 * @param array $atts Shortcode attributes.
	 *
	 * @return mixed
	 */
	public function bh_storelocator_shortcode( $atts ) {
		// Pass shortcode attributes to jQuery.
		wp_localize_script( 'storelocator-script', 'bhStoreLocatorAtts', $atts );

		$html = '';
		$filters_markup = '';

		$html .= '<div class="bh-sl-container">';
		$html .= '<div class="' . esc_html( $this->structure_option_vals['formcontainerdiv'] ) . '">';

		// Form open.
		if ( 'true' !== $this->structure_option_vals['noform'] ) {
			$html .= '<form id="' . esc_html( $this->structure_option_vals['formid'] ) . '" method="get" action="#">';
		}

		$html .= '<div class="bh-sl-form-input">';

		// Name search.
		if ( 'true' === $this->structure_option_vals['namesearch'] ) {
			$html .= '<div class="bh-sl-form-input-group">';
			$html .= '<label for="' . $this->structure_option_vals['namesearchid'] . '">' . $this->language_option_vals['namesearchlabel'] . '</label>';
			$html .= '<input type="text" id="' . $this->structure_option_vals['namesearchid'] . '" name="' . $this->structure_option_vals['namesearchid'] . '" />';
			$html .= '</div>';
		}

		// Address input field.
		$html .= '<div class="bh-sl-form-input-group">';
		$html .= '<label for="' . esc_html( $this->structure_option_vals['inputid'] ) . '">' . esc_html( $this->language_option_vals['addressinputlabel'] ) . '</label>';
		$html .= '<input placeholder="" type="text" id="' . esc_html( $this->structure_option_vals['inputid'] ) . '" name="' . esc_html( $this->structure_option_vals['inputid'] ) . '" />';
		$html .= '</div>';

		// Maximum distance.
		if ( 'true' === $this->structure_option_vals['maxdistance'] && null !== $this->structure_option_vals['maxdistvals'] ) {
			$html .= '<div class="bh-sl-form-input-group">';
			$html .= '<label for="' . $this->structure_option_vals['maxdistanceid'] . '">' . $this->language_option_vals['maxdistancelabel'] . '</label>';

			$distance_vals = explode( ',', $this->structure_option_vals['maxdistvals'] );
			if ( 'm' === $this->primary_option_vals['lengthunit'] ) {
				$dist_lang = $this->language_option_vals['mileslang'];
			} else {
				$dist_lang = $this->language_option_vals['kilometerslang'];
			}

			$html .= '<select id="' . $this->structure_option_vals['maxdistanceid'] . '" name="' . $this->structure_option_vals['maxdistanceid'] . '">';

			foreach ( $distance_vals as $distance ) {
				$html .= '<option value="' . $distance . '">' . $distance . ' ' . $dist_lang . '</option>';
			}

			$html .= '</select>';
			$html .= '</div>';
		}

		// Region selection.
		if ( 'true' === $this->structure_option_vals['region'] && null !== $this->structure_option_vals['regionvals'] ) {
			$region_vals = explode( ',', $this->structure_option_vals['regionvals'] );

			$html .= '<div class="bh-sl-form-input-group">';
			$html .= '<label for="' . $this->structure_option_vals['regionid'] . '">' . $this->language_option_vals['regionlabel'] . '</label>';
			$html .= '<select id="' . $this->structure_option_vals['regionid'] . '" name="' . $this->structure_option_vals['regionid'] . '">';

			foreach ( $region_vals as $region ) {
				$html .= '<option value="' . $region . '">' . $region . '</option>';
			}

			$html .= '</select>';
			$html .= '</div>';
		}

		$html .= '</div>';

		$html .= '<button id="bh-sl-submit" type="submit">' . $this->language_option_vals['submitbtnlabel'] . '</button>';

		// Geocode button.
		if ( ( isset( $this->structure_option_vals['geocodebtn'] ) && 'true' === $this->structure_option_vals['geocodebtn'] ) && isset( $this->structure_option_vals['geocodebtnid'] ) && isset( $this->structure_option_vals['geocodebtnlabel'] ) ) {
			$html .= '<button id="' . $this->structure_option_vals['geocodebtnid'] . '" class="bh-sl-geolocation">' . $this->structure_option_vals['geocodebtnlabel'] . '</button>';
		}

		// Include filter markup.
		if ( ! isset( $atts['filters'] ) ) {
			// Set the filters markup to a variable so it can be added as a filter value.
			$filters_markup = $this->bh_storelocator_shortcode_filters_setup();
			$html .= $filters_markup;
		}

		// Form close.
		if ( 'true' !== $this->structure_option_vals['noform'] ) {
			$html .= '</form>';
		}

		$html .= '</div>';
		$html .= '<div id="bh-sl-map-container" class="bh-sl-map-container">';
		$html .= '<div id="' . esc_html( $this->structure_option_vals['mapid'] ) . '" class="bh-sl-map"></div>';
		$html .= '<div class="' . esc_html( $this->structure_option_vals['listdiv'] ) . '">';
		$html .= '<ul class="list"></ul>';
		$html .= '</div>'; // End listdiv.

		// Pagination.
		if ( 'true' === $this->primary_option_vals['pagination'] ) {
			$html .= '<div class="bh-sl-pagination-container">';
			$html .= '<ol class="bh-sl-pagination"></ol>';
			$html .= '</div>';
		}

		$html .= '</div>'; // End bh-sl-map-container.
		$html .= '</div>'; // End bh-sl-container.

		return apply_filters( 'bh_sl_shortcode', $html, $filters_markup, $this->structure_option_vals, $this->language_option_vals );
	}

	/**
	 * Filters shortcode
	 *
	 * Adding an additional shortcode for the filters so they can be placed in another location if needed.
	 *
	 * @return mixed
	 */
	public function bh_storelocator_filters_shortcode() {
		$html = '';

		// Include filter markup.
		$html .= $this->bh_storelocator_shortcode_filters_setup();

		return apply_filters( 'bh_sl_filters_shortcode', $html, $this->structure_option_vals, $this->language_option_vals );
	}

	/**
	 * Single map shortcode
	 *
	 * @param array $atts Shortcode attributes.
	 *
	 * @return mixed
	 */
	public function bh_storelocator_single_map_shortcode( $atts ) {
		global $post;
		$html = '';
		$zoom = 16;

		if ( 'object' !== gettype( $post ) || ! is_single() ) {
			return $html;
		}

		$post_meta = get_post_meta( $post->ID );
		$lat = '';
		$lng = '';
		$terms = array();
		$cats = '';

		// Get the latitude from the location meta.
		if ( isset( $post_meta['latitude'][0] ) ) {
			$lat = $post_meta['latitude'][0];
		} else if ( isset( $post_meta['bh_storelocator_location_lat'][0] ) ) {
			$lat = $post_meta['bh_storelocator_location_lat'][0];
		}

		// Get the longitude from the location meta.
		if ( isset( $post_meta['longitude'][0] ) ) {
			$lng = $post_meta['longitude'][0];
		} else if ( isset( $post_meta['bh_storelocator_location_lng'][0] ) ) {
			$lng = $post_meta['bh_storelocator_location_lng'][0];
		}

		if ( empty( $lat ) || empty( $lng ) ) {
			return $html;
		}

		// Get the taxonomy terms.
		if ( BH_Store_Locator::BH_SL_CPT === get_post_type() ) {
			$tax_terms = get_the_terms( $post->ID, BH_Store_Locator::BH_SL_TAX );

			if ( is_array( $tax_terms ) ) {
				foreach ( $tax_terms as $tax_term ) {
					array_push( $terms, $tax_term->name );
				}

				if ( count( $terms ) > 0 ) {
					$cats = implode( ',', $terms );
				}
			}
		}

		// Check for zoom attribute.
		if ( isset( $atts['zoom'] ) && is_integer( intval( $atts['zoom'] ) ) ) {
			$zoom = intval( $atts['zoom'] );
		}

		$html = '<div id="bh-sl-map-shortcode" class="bh-sl-map-shortcode" data-lat="' . esc_attr( $lat ) . '" data-lng="' . esc_attr( $lng ) . '" data-zoom="' . absint( $zoom ) . '" data-cats="' . esc_attr( $cats ) . '"></div>';

		return apply_filters( 'bh_sl_single_map_shortcode', $html );
	}
}
